const mysql = require("mysql");
const config = require("./config");
const express = require('express')
const app = express();
const nodemailer = require('nodemailer');
const bodyParser = require('body-parser');
const router = express.Router();
const request = require("request");

app.use(bodyParser.json()); // support json encoded bodies
//app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


//#region sql connection
let con = mysql.createConnection({
    host: config.mysqlhost,
    user: config.mysqlusername,
    password: config.mysqlpassword,
    database: config.mysqldatabase,
    port: config.mysqlport
});
con.connect();
//#endregion

//#region iplogger - sends mail if someone tries to visit the root of the api domain
app.get('/', function (req, res) {
    res.send('dont event think about it your ip just has been send away to the devs if you try this again you will get in trouble!');
    let ip;
    if (req.headers['x-forwarded-for']) {
        ip = req.headers['x-forwarded-for'].split(",")[0];
    } else if (req.connection && req.connection.remoteAddress) {
        ip = req.connection.remoteAddress;
    } else {
        ip = req.ip;
    }

    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: config.gmailuser,
            pass: config.gmailpass
        }
    });

    let mail = {
        from: config.gmailuser,
        to: config.targetmail,
        subject: "Silvan Paul - Someone tried to get in to the api!",
        text: "Hi admin! a visitor with the ip adress: " + ip + " tried possibly to hack/pentest your site! want to take action?"
    }


    transporter.sendMail(mail, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
    console.log("look silvan its another hacker boii scrub have fun!: " + ip);
});
//#endregion

//#region REST REQUESTS - only read actions no create and update and delete yet...

//#region user data - GET
app.get('/users/:username', function (req, res) {
    //some headers for access to api
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Content-Type: application/vnd.api+json");
    //query for database server
    con.query('select id, username, isAdmin, created, email, ipadress from users where username=?', [req.params.username], function (error, results, fields) {
        if (error) {
            res.json(error);
        }
        for (let i = 0; i < results.length; i++) {
            res.json({
                user: {
                    id: results[i].id,
                    username: results[i].username,
                    isAdmin: results[i].isAdmin,
                    created: results[i].created,
                    email: results[i].email,
                    ipadress: results[i].ipadress
                }
            });
        }
    });
});
//#endregion

//#region user data -POST

//#endregion

//#region homepage data - GET
app.get('/homeitems/:id', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Content-Type: application/vnd.api+json");


    con.query("SELECT * FROM homepage WHERE id=" + [req.params.id], function (error, results, fields) {
        if (error) {
            res.send(error);
        }
        for (let i = 0; i < results.length; i++) {
            res.json({
                homeitem: {
                    id: results[i].id,
                    title: results[i].title,
                    text: results[i].text
                }
            });
        }
    });
});
//#endregion

//#region navigation items - GET
app.get('/navitems', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Content-Type: application/vnd.api+json");
    con.query("SELECT id, item, link, isVisible FROM navitems", (error, navitems, fields) => {
        if (error) {
            res.status(404).end();
        }
        res.json({
            navitems
        });
    });
});
//#endregion


//#region project items - GET
app.get('/projects', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Content-Type: application/vnd.api+json");
    con.query("SELECT * FROM projects", (error, projects, fields) => {
        if (error) {
            res.status(404).end();
        }
        res.json({
            projects
        });
    });
});
//#endregion

//#region about text - GET
app.get('/abouts', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Content-Type: application/vnd.api+json");
    con.query("SELECT * FROM about", (error, about, fields) => {
        if (error) {
            res.status(404).end();
        }
        res.json({
            about
        });
    });
});
//#endregion

app.get('/status/systems', (req, res) => {
    request('https://status.silvanpaul.nl', (error, response, body) =>{
        let result = JSON.parse(body);
        // res.send(body);
        res.json(result);
    })
});

app.get('/status/monitorstats', (req, res) => {
    request('https://status.silvanpaul.nl', (error, response, body) =>{
        let result = JSON.parse(body);
        // res.send(body);
        res.json(result.monit);
    })
});

app.get('/status/systems', (req, res) => {
    request('https://status.silvanpaul.nl', (error, response, body) =>{
        let result = JSON.parse(body);
        // res.send(body);
        res.json(result.processes[0]);
    })
});

app.listen(config.port);
console.log("listening to port: " + config.port);