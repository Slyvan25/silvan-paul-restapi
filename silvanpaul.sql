-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 24, 2018 at 09:36 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `silvanpaul`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `text1` text NOT NULL,
  `title2` varchar(255) NOT NULL,
  `text2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title1`, `text1`, `title2`, `text2`) VALUES
(1, 'Introductie', 'Dag bezoeker! mijn naam is silvan paul en ik ben geboren op 1 oktober 1999. ik maak al sites sinds kleins af aan en ik ben er alleenmaar beter in geworden met natuurlijk ook veel hulp van w3schools (ik denk dat heel veel mensen daar wel de basics vandaan halen zo niet dan is een kijkje daar echt een aanrader). mijn hobby is dan natuurlijk ook sites apps en games maken qua het ict gebied verder vind ik het leuk om met servers te spelen en los van ict dingen me bezig te houden met het maken en lusiteren van muziek.\r\n\r\nik speel nu al vanaf groep 6 op het drumstel en ik draai al vanaf dat ik in groep 8 zit. verder vind ik het ook wel leuk soms om piano te spelen en gitaar te spelen(ook al ben ik dat best wel verleerd). ik produceer als ik tijd heb nog wel ik ben op dit moment erg bezig met het genre hardcore en hardstyle qua produceren omdat ik die het leukst vind om te maken. heel afentoe produceer ik wat deephouse maar ik ben vaak niet tevreden dus daarom breng ik niet echt meer iets uit.\r\n\r\nVerder vind ik fotograferen ook leuk om te doen. ik heb een al wat oudere compact camera namelijk de \'nikon coolpix p100\'. ik fotografeer al sinds dat ik klein ben omdat mijn opa me al vroeg een kodak gaf en ik die natuurlijk heel mooi vond en heel veel later is die helaas gesneuveld van slijtage en toen is mijn focus heel lang filmen geweest.\r\n\r\nfilmpjes editen vind ik ook erg leuk om te doen daarom heb ik heel lang een eigen youtube kanaal gehad die natuurlijk super dom was maar wel leuk voor toen, ik heb nogsteeds een youtube kanaal maar daar post ik alleen wat dingen waarmee ik dan bezig ben en dat engels is(https://www.youtube.com/channel/UCVUoKUgFUuph6qCYRWk3vvA).\r\n', 'Coding en programeren', 'Nu ik je mijn saaie levensverhaal heb verteld zal ik wel gewoon je vertellen wat ik leuk vind op het ict gebied.\r\n\r\nik maak graag sites met nodejs.. waarom? omdat ik php minder leuk vind om te doen de taal is niet de favoriet bij mij omdat ik javascript veel makkelijkeer vind voor async en sockets.\r\n\r\nlos van websites vind ik het ook leuk om applicaties te maken dat doe ik bijvoorbeeld met c# of C++ maar soms ook met nodejs aangezien je tegenwoordig alles makkelijk kan builden voor meerdere platformen met cardovajs.\r\n\r\nik werk sinds kort met ember js voor mijn stage en die kennis zal ik als het kan niet weg laten zakken (deze site is ook gemaakt met emberjs).\r\nik vind emberjs best prettig werken en het heeft best wel een mooie mvc logica.');

-- --------------------------------------------------------

--
-- Table structure for table `homepage`
--

CREATE TABLE `homepage` (
  `id` int(255) NOT NULL,
  `title` varchar(125) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homepage`
--

INSERT INTO `homepage` (`id`, `title`, `text`) VALUES
(1, 'Kan ik een aanvraag doen voor een site?', 'Als u wil dat ik een site voor u maak dan kunt u de contact gegevens gebruiken die te vinden zijn via de navigatie balk. ik zal dan zo snel mogelijk reageren en de prijs is dan nader overeen te komen aangezien elke site verschillend is.\r\n\r\nIk werk met Nodejs, EmberJS en ben een groote fan van hoe sites tegenwoordig meer \"client sided\" worden.\r\n\r\nmijn motto: alles is mogelijk zolang de browser en pc het ook kan.');

-- --------------------------------------------------------

--
-- Table structure for table `navitems`
--

CREATE TABLE `navitems` (
  `id` int(11) NOT NULL,
  `item` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `isVisible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `navitems`
--

INSERT INTO `navitems` (`id`, `item`, `link`, `isVisible`) VALUES
(1, 'Projecten', 'projects', 1),
(2, 'Over mij', 'about', 1),
(3, 'Contact', 'contact', 1),
(4, 'Login', 'login', 0);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `text` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `isVisible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title`, `text`, `url`, `img`, `isVisible`) VALUES
(1, 'Panfu HTML5', 'Ik wou al heel lang iets doen met html5 canvas.. toen ik het op school kreeg ben ik maar bezig gegaan met een eigen versie van panfu helemaal flashplayer vrij helaas is dit project niet helemaal af', 'panfu.silvanpaul.nl', 'panfu.png', 1),
(2, 'SpellenBoot', 'Toen het eind van het schooljaar in zicht was had ik een idee om een spelletjes site te maken met nodejs helaas had ik geen rekening gehouden met de \"publisher rights\".', 'spellenboot.nl', 'spellenboot.png', 1),
(3, 'Mixed', 'mixed is een applicatie gericht op djs die draaien in clubs, als hobby etc. ik maak deze applicatie omdat ik graag wil kunnen draaien op low end en oude computers zonder veel te hoeven betalen.', 'mixeddemo.silvanpaul.com', 'mixed.png', 1),
(4, 'CoffeeToGo', 'Coffee ToGo wat beter bekend stond als freetogo was een project van mij en een vriend van me (Jonathan) om gratis koffie te verkrijgen bij de ah togo. helaas werd het gestres test door iemand en sloeg alles op hol . Daardoor maakte we perrongeluk een hoog aantal accounts aan waardoor de actie stop is gezet door albert heijn.', 'freetogo.coffee', 'coffeetogo.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `created` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `ipadress` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homepage`
--
ALTER TABLE `homepage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `navitems`
--
ALTER TABLE `navitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `homepage`
--
ALTER TABLE `homepage`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `navitems`
--
ALTER TABLE `navitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
